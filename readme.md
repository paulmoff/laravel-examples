## Laravel Examples App

### Instructions

Copy `.env.example` to `.env` and make sure you have LINKEDIN_CLIENT, LINKEDIN_SECRET, LINKEDIN_CALLBACK variables set up()

Run `php artisan key:generate`

Install dependencies using `composer install`

Set your database credentials in `.env` file and run `php artisan migrate`
 

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
