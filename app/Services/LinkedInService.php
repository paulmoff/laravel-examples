<?php

namespace App\Services;

use GuzzleHttp\Client;

class LinkedInService
{
    public function getAccess()
    {
        if (request()->has('error'))
            throw new \Exception(urldecode(request('error_description')));

        $authCode = request('code');
        $state = request('state');

        // CSRF check
        if ($state != session()->get('state'))
            throw new \Exception('CSRF attack detected!');

        $params = [
            'grant_type' => 'authorization_code',
            'code' => $authCode,
            'redirect_uri' => config('services.linkedin.redirect_uri'),
            'client_id' => config('services.linkedin.client'),
            'client_secret' => config('services.linkedin.secret'),
        ];

        $client = $this->getClient();

        $response = $client->request('POST', 'https://www.linkedin.com/oauth/v2/accessToken', [
            'form_params' => $params,
        ]);

        $data = json_decode($response->getBody());

        // save access token
        auth()->user()->setLinkedInToken($data->access_token);
    }

    private function getClient($options = []) {

        if (config('services.linkedin.proxy')) {
            $options = [
                'proxy' => config('services.linkedin.proxy'),
            ];
        }

        return new Client($options);
    }
}