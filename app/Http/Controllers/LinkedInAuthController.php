<?php

namespace App\Http\Controllers;

use App\Services\LinkedInService;

class LinkedInAuthController extends Controller
{
    public function index()
    {
        return view('linkedin.index');
    }

    public function requestCode()
    {
        session()->put('state', $state = str_random());

        $url = 'https://www.linkedin.com/oauth/v2/authorization?'
            . 'response_type=code'
            . '&client_id=' . config('services.linkedin.client')
            . '&redirect_uri=' . urlencode(config('services.linkedin.redirect_uri'))
            . '&state=' . $state;

        return redirect()->away($url);
    }

    public function callback()
    {
        $linkedInService = resolve(LinkedInService::class);

        try {
            $linkedInService->getAccess();
            return redirect(route('linkedin'))->with('success', 'Access token saved!');
        } catch (\Exception $e) {
            return redirect(route('linkedin'))->with('error', $e->getMessage());
        }
    }
}
