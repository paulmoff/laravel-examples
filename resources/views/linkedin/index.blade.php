@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>LinkedIn Auth Example</h3>
                    </div>
                    <div class="card-body">
                        @if(auth()->user()->linkedin_token)
                            <p>You have access token stored. Update?</p>
                            <p><a href="{{ route('linkedin.requestCode') }}" class="btn btn-success">Update token</a></p>
                        @else
                            <p>Let's get your LinkedIn API access token!</p>
                            <p><a href="{{ route('linkedin.requestCode') }}" class="btn btn-success">Get token</a></p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
